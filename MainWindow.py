# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from checkmypass import data_return_to_gui
from sitebreach import return_all_data_to_gui, remove_html_tags


class PassListModel(QtCore.QAbstractListModel):
    def __init__(self, *args, passwd=None, **kwargs):
        super(PassListModel, self).__init__(*args, **kwargs)
        self.passwd = passwd or []

    def data(self, index, role):
        if role == QtCore.Qt.DisplayRole:
            password, text = self.passwd[index.row()]
            return password

    def rowCount(self, index):
        return len(self.passwd)


class Ui_MainWindow(object):
    model = PassListModel()

    def pb_CMP_clicked(self):
        query = self.lineEdit_CMP.text()
        query = query.split(' ')
        response = data_return_to_gui(query)
        model = PassListModel()
        self.listView_CMP.setModel(model)
        for e in response:
            model.passwd.append(e)
        model.layoutChanged.emit()
        self.lineEdit_CMP.setText('')

    def pb_SB_clicked(self):
        query = self.lineEdit_SB.text()
        query = query.split(' ')
        data = return_all_data_to_gui(query)
        print(data)
        print(type(data))

    def setupUi(self, MainWindow):



        # Setup Main Window
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(642, 774)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(642, 774))
        MainWindow.setMaximumSize(QtCore.QSize(642, 774))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        MainWindow.setWindowTitle('CMP&SB')

        # Setup tab Widget
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(0, 0, 642, 724))
        self.tabWidget.setTabBarAutoHide(False)
        self.tabWidget.setObjectName("tabWidget")

        # Setup first tab (Check my password)
        self.tab_CMP = QtWidgets.QWidget()
        self.tab_CMP.setObjectName("tab_CMP")
        self.gridLayoutWidget = QtWidgets.QWidget(self.tab_CMP)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(20, 0, 601, 80))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.label_CMP = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_CMP.setMouseTracking(False)
        self.label_CMP.setStyleSheet("font: 11pt \"Arial\";")
        self.label_CMP.setTextFormat(QtCore.Qt.PlainText)
        self.label_CMP.setScaledContents(False)
        self.label_CMP.setObjectName("label_CMP")
        self.gridLayout.addWidget(self.label_CMP, 0, 0, 1, 1)
        self.lineEdit_CMP = QtWidgets.QLineEdit(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lineEdit_CMP.setFont(font)
        self.lineEdit_CMP.setObjectName("lineEdit_CMP")
        self.gridLayout.addWidget(self.lineEdit_CMP, 0, 1, 1, 1)
        self.pb_CMP = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.pb_CMP.setFont(font)
        self.pb_CMP.setObjectName("pb_CMP")
        self.gridLayout.addWidget(self.pb_CMP, 0, 2, 1, 1)
        self.listView_CMP = QtWidgets.QListView(self.tab_CMP)
        self.listView_CMP.setGeometry(QtCore.QRect(20, 90, 171, 591))
        self.listView_CMP.setObjectName("listView_CMP")
        self.listView_CMP.setModel(self.model)
        self.textBrowser_CMP = QtWidgets.QTextBrowser(self.tab_CMP)
        self.textBrowser_CMP.setEnabled(True)
        self.textBrowser_CMP.setGeometry(QtCore.QRect(200, 90, 421, 591))
        self.textBrowser_CMP.setAcceptDrops(False)
        self.textBrowser_CMP.setOpenLinks(False)
        self.textBrowser_CMP.setObjectName("textBrowser_CMP")
        self.tabWidget.addTab(self.tab_CMP, "")

        # Setup second tab (Site Breach)
        self.tab_SB = QtWidgets.QWidget()
        self.tab_SB.setObjectName("tab_SB")
        self.gridLayoutWidget_3 = QtWidgets.QWidget(self.tab_SB)
        self.gridLayoutWidget_3.setGeometry(QtCore.QRect(20, 0, 601, 80))
        self.gridLayoutWidget_3.setObjectName("gridLayoutWidget_3")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.gridLayoutWidget_3)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label_SB = QtWidgets.QLabel(self.gridLayoutWidget_3)
        self.label_SB.setEnabled(True)
        self.label_SB.setMinimumSize(QtCore.QSize(166, 0))
        self.label_SB.setMaximumSize(QtCore.QSize(166, 16777215))
        self.label_SB.setMouseTracking(False)
        self.label_SB.setStyleSheet("font: 11pt \"Arial\";")
        self.label_SB.setTextFormat(QtCore.Qt.PlainText)
        self.label_SB.setScaledContents(False)
        self.label_SB.setObjectName("label_SB")
        self.gridLayout_3.addWidget(self.label_SB, 0, 0, 1, 1)
        self.lineEdit_SB = QtWidgets.QLineEdit(self.gridLayoutWidget_3)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lineEdit_SB.setFont(font)
        self.lineEdit_SB.setObjectName("lineEdit_SB")
        self.gridLayout_3.addWidget(self.lineEdit_SB, 0, 1, 1, 1)
        self.pb_SB = QtWidgets.QPushButton(self.gridLayoutWidget_3)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.pb_SB.setFont(font)
        self.pb_SB.setObjectName("pb_SB")
        self.gridLayout_3.addWidget(self.pb_SB, 0, 2, 1, 1)
        self.textBrowser_SB = QtWidgets.QTextBrowser(self.tab_SB)
        self.textBrowser_SB.setGeometry(QtCore.QRect(200, 90, 421, 591))
        self.textBrowser_SB.setAcceptDrops(False)
        self.textBrowser_SB.setOpenLinks(False)
        self.textBrowser_SB.setObjectName("textBrowser_SB")
        self.listView_SB = QtWidgets.QListView(self.tab_SB)
        self.listView_SB.setGeometry(QtCore.QRect(20, 90, 171, 591))
        self.listView_SB.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.listView_SB.setProperty("showDropIndicator", True)
        self.listView_SB.setObjectName("listView_SB")
        self.tabWidget.addTab(self.tab_SB, "")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 642, 26))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.statusbar.sizePolicy().hasHeightForWidth())
        self.statusbar.setSizePolicy(sizePolicy)
        self.statusbar.setSizeGripEnabled(False)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionAbout = QtWidgets.QAction(MainWindow)
        self.actionAbout.setObjectName("actionAbout")
        self.actionQuit = QtWidgets.QAction(MainWindow)
        self.actionQuit.setObjectName("actionQuit")
        self.menuFile.addAction(self.actionAbout)
        self.menuFile.addAction(self.actionQuit)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.pb_CMP.clicked.connect(self.pb_CMP_clicked)
        self.pb_SB.clicked.connect(self.pb_SB_clicked)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_CMP.setText(_translate("MainWindow", "Check my password:"))
        self.pb_CMP.setText(_translate("MainWindow", "Check\nPassword"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_CMP), _translate("MainWindow", "Check Password"))
        self.label_SB.setText(_translate("MainWindow", "Check site search:"))
        self.pb_SB.setText(_translate("MainWindow", "Check\nSite"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_SB), _translate("MainWindow", "Check Site"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.actionAbout.setText(_translate("MainWindow", "About"))
        self.actionQuit.setText(_translate("MainWindow", "Quit"))



