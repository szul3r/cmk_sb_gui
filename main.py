import MainWindow
import sys
from PyQt5 import QtWidgets

app = QtWidgets.QApplication(sys.argv)
mw = QtWidgets.QMainWindow()
ui = MainWindow.Ui_MainWindow()
ui.setupUi(mw)
mw.show()


sys.exit(app.exec_())

